import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyApp {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(MyApp.class.getName());
        Scanner sc = new Scanner(System.in);
        MyBigNumber myBigNumber = new MyBigNumber();
        String number1;
        String number2;
        String result;

        System.out.print("Nhap so thu 1: ");
        number1 = sc.nextLine();
        System.out.print("Nhap so thu 2: ");
        number2 = sc.nextLine();

        result = myBigNumber.sum(number1, number2);
        logger.log(Level.INFO, "Result: {0}", result);

        sc.close();
    }
}
