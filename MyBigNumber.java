/**
 * MyBigNumber
 */
public class MyBigNumber {

    public String add0(int index, String number) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < index; i++) {
            builder.insert(0, "0");
        }
        builder.append(number);
        return builder.toString();
    }

    public String sum(String number1, String number2) {
        int over = 0;
        String presentResult = "";
        String result = "";
        int howMuchLonger;

        howMuchLonger = number1.length() - number2.length();
        if (howMuchLonger > 0) {
            number2 = add0(howMuchLonger, number2);
        } else {
            number1 = add0(howMuchLonger * -1, number1);
        }

        for (int i = number1.length(); i > 0; i--) {
            int num1 = Integer.parseInt(String.valueOf(number1.charAt(i - 1)));
            int num2 = Integer.parseInt(String.valueOf(number2.charAt(i - 1)));

            int resultNum = num1 + num2 + over;

            over = resultNum / 10;

            System.out.println("________");
            presentResult = String.valueOf(resultNum - 10 * over);
            result = presentResult + result;
        }

        if (over > 0) {
            result = String.valueOf(over) + result;
        }

        return result;
    }
}